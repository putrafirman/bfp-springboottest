package com.mastahcode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mastahcode.model.Mahasiswa;
import com.mastahcode.service.MahasiswaService;

@Controller
public class MahasiswaController {
	private MahasiswaService mhsService;
		
	@Autowired
	public void setMhsService(MahasiswaService mhsService) {
		this.mhsService = mhsService;
	}


	@RequestMapping("/mhs")
	public String home(Model model){
		model.addAttribute("mahasiswa", mhsService.listMahasiswa());
		model.addAttribute("mhs", new Mahasiswa());	
		return "mahasiswa";
	}
	
	@RequestMapping(value = "/mhs/create", method = RequestMethod.POST)
	public String simpan(Model model, Mahasiswa mhs){
		model.addAttribute("mahasiswa", mhsService.SaveOrUpdate(mhs));
		return "redirect:/mhs";
	}
	
	@RequestMapping(value = "mhs/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable Integer id, Model model){
		model.addAttribute("mahasiswa", mhsService.getById(id));
		return "form";
	}
	@RequestMapping(value = "mhs/hapus/{id}", method = RequestMethod.GET)
	public String hapus(@PathVariable Integer id){
		mhsService.hapusMahasiswa(id);
		return "redirect:/mhs";
	}
}
