package com.mastahcode.service;

import java.util.List;

import com.mastahcode.model.Mahasiswa;

public interface MahasiswaService {
	List<Mahasiswa> listMahasiswa();
	Mahasiswa SaveOrUpdate(Mahasiswa mhs);
	Mahasiswa getById(Integer id);
	void hapusMahasiswa(Integer id);
}
