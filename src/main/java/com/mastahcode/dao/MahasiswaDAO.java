package com.mastahcode.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mastahcode.model.Mahasiswa;
import com.mastahcode.service.MahasiswaService;

@Service
public class MahasiswaDAO implements MahasiswaService {
	private EntityManagerFactory emf;
	@Autowired
	public void setEntitiManagerFactory(EntityManagerFactory emf){
		this.emf = emf;
	}
	
	@Override
	public List<Mahasiswa> listMahasiswa() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Mahasiswa", Mahasiswa.class).getResultList();
		
	}

	@Override
	public Mahasiswa SaveOrUpdate(Mahasiswa mhs) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Mahasiswa saved = em.merge(mhs);
		em.getTransaction().commit();
		return saved;
	}

	@Override
	public Mahasiswa getById(Integer id) {
		EntityManager em = emf.createEntityManager();
		return em.find(Mahasiswa.class, id);		
	}

	@Override
	public void hapusMahasiswa(Integer id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.find(Mahasiswa.class, id));
		em.getTransaction().commit();
	}
	

}
